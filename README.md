# overview

## Why?

essentially, creating lots of objects can be really slow/inefficient. this does that beforehand, so you have some "waiting" when you need them.

## please also refer to the documentation for `WCSpawnPool`

- in idle `_process`:
  - tries to keep `thres_in_memory` instances of `scene_file` in memory (in a background WorkerThread)
	- this runs `_init` and `preload` and all that
  - tries to keep `thres_in_tree` instances in the SceneTree (1 per frame, because we can't do this in the background!)
	- as child of a non-processing Node
	- with its `visibility` property set to `false` if it exists
	- this runs `_ready` etc
- `spawn(callable = null)`:
  - makes sure one is available (blocking/awaiting as required)
  - reparents it to `spawn_parent` (if set)
  - makes it visible again if we previously hid it
  - emits `spawned(Node)`
  - calls `callable(Node)` if not-null
  - returns the node.
- `despawn(node)`: returns an instance to the in-tree pool
- `int in_tree`: how many are currently pooled in-tree
- `int in_memory`: how many are currently pooled in-memory

# instructions

- installation:
  - copy "addons" folder to your project (dont rename anything, put right in `res://`, no subfolders, merge if already one there)
  - go to ProjectSettings -> Plugins and enable
- usage:
  - put a `WCSpawnPool` node in a scene
  - tell it in `scene_file` which scene to spawn (just drop the tscn file in from the filesystem dock in the editor)
  - tell it in `thres_*` how many to keep in the 2 pools
  - tell it in `spawn_parent` the parent to add them to (just click assign and select a node from the scene the pool is in)
  - call `spawn` whenever you want an instance, in any of those ways:

```gdscript
# the fire-and-forget method
the_pool.spawn()

# upside: the easiest, does not block or anything, just adds the instance to `spawn_parent` when done
# downside: the outside/caller doesn't see it happen
```

```gdscript
# the async await method
var the_scene := await the_pool.spawn()
do_something_with(the_scene)

# upside: easy, the result is right there
# downside: will pause the current function in the "await" if none are available in the pool
```

```gdscript
# the callback method

the_pool.spawn(do_something_with)

# with the lambda trick:
the_pool.spawn(func(the_scene: Node):
  do_something_with(the_scene)
)

# upside: also easy, will not pause there
# downside: might introduce overhead to create/pass callbacks a lot
```

```gdscript
# the signal method
the_pool.spawned.connect(do_something_with)

the_pool.spawn()

# upside: helpful if you want to react to all the spawns in one spot, also will not pause here
# downside: less versatile i guess if you want to react to them individualized.
```

(assuming that `do_something_with` is some function / code that works on `the_scene`, which is the instance we got from the pool; and that `the_pool` is the `WCSpawnPool` node)
