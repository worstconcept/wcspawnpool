extends Node2D

@onready var wc_spawn_pool: WCSpawnPool = $WCSpawnPool

var doit: bool = false
var delit: bool = false

func _process(_delta: float) -> void:
	if doit:
		var thing := await wc_spawn_pool.spawn()
		@warning_ignore("unsafe_property_access")
		thing.position = Vector2(randf_range(100,1000),randf_range(100,500))
	if delit and $Target.get_child_count()>0:
		wc_spawn_pool.despawn($Target.get_child(0))

func _on_button_toggled(toggled_on: bool) -> void:
	doit = toggled_on


func _on_button_2_toggled(toggled_on: bool) -> void:
	delit = toggled_on
